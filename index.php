<?php
$token = "ZeGlUqN2jwB2TxqQxlrXtTo6JLUMg9OE8BMVEMjSvtw";
$hasil = file_get_contents('https://api.gumroad.com/v2/products?access_token='.$token."");
$hasil = json_decode($hasil,true);

$product = [];
// foreach($hasil['products'] as $tmpProdduct){
//     $productName[] = $tmpProdduct['name'];
//     // $productImage = [];
//     // $productPrice = [];
//     // $productDesc = [];

// }
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Premium Asset Kit by Nixx    </title>
    <link rel="stylesheet" href="Style/style.css">
</head>
<body>

    <!-- This Section For Header -->
        <div class="header bg-grey shadow">
        <div class="container">
                <div class="row baseline">
                    <div class="col-flex text-left">NIXX DESIGN</div>
                    <div class="col-flex">
                        <ul class="list-none text-center">
                            <li class="active"><a href="#"> Product</a></li>
                            <li><a href="#"> Blog</a></li>
                            <li><a href="#"> Custom Icon</a></li>
                        </ul>
                    </div>
                    <div class="col-flex right"><p class="text-right contact">Contact us</p></div>
                </div>
                
            </div>
        </div>




        <!-- this section for hero banner -->
<div class="spacer-lg text-left">
    <div class="container">
        <div class="row">
            <div class="col-flex">
            <h1 class="title-lg">Premium Assets KIT For Graphic Designer</h1>
        <p class="spacer-md">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugit suscipit incidunt, cumque, repellat vero impedit non, iste sint maiores error soluta vitae similique maxime? Laboriosam eligendi repellat minus consectetur quasi!</p>
        <a href="#product">
        <div class="button-primary">
                Shop Now!
        </div>
        </a>
            </div>
            <div class="col-flex">
            <svg class="hero-illustration" width="48" height="48" version="1.1" viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg">
                <path d="m36.641 24.492c0 1.108-0.892 2-2 2h-21.282c-1.108 0-2-0.892-2-2v-16.492c0-1.108 0.892-2 2-2h21.282c1.108 0 2 0.892 2 2z" style="-inkscape-stroke:none;color:#000000;fill:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-width:2"/>
                <path d="m11.359 24.491v15.508c0 1.1081 0.892 2.0001 2 2.0001h21.282c1.108 0 2-0.892 2-2.0001v-15.508c0 1.1081-0.892 2.0002-2 2.0002h-21.282c-1.108 0-2-0.8921-2-2.0002z" style="-inkscape-stroke:none;color:#000000;fill:#80b5f5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-width:2"/>
                <path d="m11.36 24.492v2.4235c0 1.1079 0.892 2 2 2h21.281c1.108 0 2-0.8921 2-2v-2.4235c0 1.108-0.892 2-2 2h-21.281c-1.108 0-2-0.892-2-2z" style="-inkscape-stroke:none;color:#000000;fill:#6495ed;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-width:2"/>
                <path d="m31.879 31.027c-0.552 0-1 0.4477-1 1s0.448 1 1 1h2.166c0.552 0 1-0.4477 1-1s-0.448-1-1-1zm-1.707 3.1641c-0.552 0-1 0.4477-1 1s0.448 1 1 1h2.166c0.552 0 1-0.4477 1-1s-0.448-1-1-1z" style="-inkscape-stroke:none;color:#000000;fill:#6495ed;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m13.36 5c-1.645 0-3 1.3553-3 3v6.832c0 0.5523 0.447 1 1 1 0.552 0 1-0.4477 1-1v-6.832c0-0.5713 0.428-1 1-1h21.281c0.571 0 1 0.4287 1 1v19.836c0 0.5523 0.447 1 1 1 0.552 0 1-0.4477 1-1v-19.836c0-1.6447-1.356-3-3-3zm-2 15.826c-0.553 0-1 0.4477-1 1v18.174c0 1.6447 1.355 3 3 3h21.281c1.644 0 3-1.3553 3-3v-6.8242c0-0.5523-0.448-1-1-1-0.553 0-1 0.4477-1 1v6.8242c0 0.5713-0.429 1-1 1h-21.281c-0.572 0-1-0.4287-1-1v-13.225c0.313 0.1128 0.649 0.1777 1 0.1777h17.019c0.552 0 1-0.4477 1-1s-0.448-1-1-1h-17.019c-0.572 0-1-0.4287-1-1v-2.1269c0-0.5523-0.448-1-1-1z" style="-inkscape-stroke:none;color:#000000;fill:#465673;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:9.8696"/>
                <path d="m14.404 10.028v11.665c0 0.586 0.475 1.061 1.061 1.061h17.071c0.586 0 1.061-0.475 1.061-1.061v-11.665c0-0.586-0.475-1.061-1.061-1.061h-17.071c-0.586 0-1.061 0.475-1.061 1.061z" style="-inkscape-stroke:none;color:#000000;fill:#fbc086;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-width:2"/>
                <path d="m27.782 38.538c-0.391-0.3905-1.024-0.3905-1.415 0-0.39 0.3905-0.39 1.0237 0 1.4142 0.391 0.3905 1.024 0.3905 1.415 0 0.39-0.3905 0.39-1.0237 0-1.4142z" style="-inkscape-stroke:none;color:#000000;fill:#6495ed;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m30.839 39.953c0.391-0.3905 0.391-1.0237 0-1.4142-0.39-0.3905-1.023-0.3905-1.414 0-0.39 0.3905-0.39 1.0237 0 1.4142 0.391 0.3905 1.024 0.3905 1.414 0z" style="-inkscape-stroke:none;color:#000000;fill:#6495ed;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m33.897 39.953c0.391-0.3905 0.391-1.0237 0-1.4142-0.39-0.3905-1.023-0.3905-1.414 0-0.39 0.3905-0.39 1.0237 0 1.4142 0.391 0.3905 1.024 0.3905 1.414 0z" style="-inkscape-stroke:none;color:#000000;fill:#6495ed;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m14.403 11.856v-1.8269c0-0.5863 0.475-1.0617 1.062-1.0614h17.071c0.586-3e-4 1.061 0.4751 1.061 1.0614v1.8269c-1e-3 -0.5855-0.476-1.0597-1.061-1.0595h-17.071c-0.586-2e-4 -1.061 0.474-1.062 1.0595z" style="-inkscape-stroke:none;color:#000000;fill:#f6956f;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-width:2"/>
                <path d="m22.017 30.839c-1.197-1.1965-3.168-1.1965-4.364 0l-3.249 3.2484c-1.196 1.1964-1.196 3.1674 0 4.364 1.197 1.1967 3.168 1.1967 4.365 0l3.248-3.2483c1.197-1.1965 1.196-3.1674 0-4.3641z" style="-inkscape-stroke:none;color:#000000;fill:#4c84df;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m22.871 33.513c-0.098 0.6205-0.379 1.2173-0.853 1.6913l-3.25 3.248c-1.197 1.1966-3.167 1.1966-4.364 0-0.474-0.4739-0.757-1.0707-0.855-1.6914-0.15 0.9464 0.133 1.9513 0.855 2.6739 1.197 1.1966 3.167 1.1966 4.364 0l3.25-3.2481c0.722-0.7226 1.003-1.7274 0.853-2.6737z" style="-inkscape-stroke:none;color:#000000;fill:#6495ed;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m17.354 35.502c0.433 0.4325 0.433 1.1035 0 1.5358-0.437 0.4377-1.098 0.4377-1.535 0-0.438-0.4376-0.438-1.0981 0-1.5356 0.432-0.4323 1.103-0.4325 1.535-2e-4z" style="-inkscape-stroke:none;color:#000000;fill:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m20.603 32.254c0.437 0.4376 0.437 1.098 0 1.5356-0.433 0.4326-1.104 0.4326-1.536 1e-4 -0.433-0.4323-0.433-1.1033 0-1.5357 0.438-0.4377 1.098-0.4376 1.536 0z" style="-inkscape-stroke:none;color:#000000;fill:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m30.667 32.027h2.165z" style="-inkscape-stroke:none;color:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-width:2"/>
                <path d="m28.959 35.053h2.166z" style="-inkscape-stroke:none;color:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-width:2"/>
                <path d="m30.666 31.027c-0.552 0-1 0.4477-1 1s0.448 1 1 1h2.166c0.552 0 1-0.4477 1-1s-0.448-1-1-1z" style="-inkscape-stroke:none;color:#000000;fill:#4c84df;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m28.959 34.192c-0.552 0-1 0.4477-1 1s0.448 1 1 1h2.166c0.552 0 1-0.4477 1-1s-0.448-1-1-1z" style="-inkscape-stroke:none;color:#000000;fill:#4c84df;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m11.359 18.214v0" style="-inkscape-stroke:none;color:#000000;fill:#f4f6f8;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:9.8696;stroke-width:2"/>
                <path d="m11.359 17.215a1 1 0 0 0-1 1 1 1 0 0 0 1 1 1 1 0 0 0 1-1 1 1 0 0 0-1-1z" style="-inkscape-stroke:none;color:#000000;fill:#465673;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:9.8696"/>
                <path d="m25.159 19.701c-0.391-0.3905-1.024-0.3905-1.414 0-0.391 0.3905-0.391 1.0237 0 1.4142 0.39 0.3905 1.023 0.3905 1.414 0 0.39-0.3905 0.39-1.0237 0-1.4142z" style="-inkscape-stroke:none;color:#000000;fill:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m28.217 21.115c0.39-0.3905 0.39-1.0237 0-1.4142-0.391-0.3905-1.024-0.3905-1.414 0-0.391 0.3905-0.391 1.0237 0 1.4142 0.39 0.3905 1.023 0.3905 1.414 0z" style="-inkscape-stroke:none;color:#000000;fill:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m31.275 21.115c0.39-0.3905 0.39-1.0237 0-1.4142-0.391-0.3905-1.024-0.3905-1.415 0-0.39 0.3905-0.39 1.0237 0 1.4142 0.391 0.3905 1.024 0.3905 1.415 0z" style="-inkscape-stroke:none;color:#000000;fill:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m17.402 12.424c-0.39-0.3905-1.023-0.3905-1.414 0-0.39 0.3905-0.39 1.0237 0 1.4142 0.391 0.3905 1.024 0.3905 1.414 0 0.391-0.3905 0.391-1.0237 0-1.4142z" style="-inkscape-stroke:none;color:#000000;fill:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
                <path d="m20.078 12.311h4.718z" style="-inkscape-stroke:none;color:#000000;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;stroke-width:2"/>
                <path d="m20.079 12.131c-0.552 0-1 0.4477-1 1s0.448 1 1 1h4.717c0.552 0 1-0.4477 1-1s-0.448-1-1-1z" style="-inkscape-stroke:none;color:#000000;fill:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10"/>
            </svg>

            </div>

        </div>
    </div>
</div>




<!-- this section for featured product -->
<div class="section bg-grey">
    <div class="container text-center center spacer margin-content">
        <h2 class="title-md">
            Featured
        </h2>
    <div class="center list-style"></div>
    <div class="container margin-top">
        <div class="col-3">
            <div class="hero-icon">
                <svg enable-background="new 0 0 128 128" height="128px" id="Layer_1" version="1.1" viewBox="0 0 128 128" width="128px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M89.441,33.805H38.59c-1.968,0-3.568-1.6-3.568-3.565V14.006h57.986V30.24C93.008,32.206,91.408,33.805,89.441,33.805z    M39.021,29.805h49.986V18.006H39.021V29.805z"/><g><path d="M64.164,99.415l-1.965-4.259c-0.075-0.163-7.718-16.506-24.341-33.128l-1.124-1.124l0.841-1.349    C48.172,42.561,48.183,32.4,48.182,32.299l4-0.074c0.009,0.441,0.097,10.766-10.384,28.098    c11.877,12.141,19.003,23.753,22.142,29.479c2.847-5.479,9.438-16.377,22.238-29.427c-10.32-15.537-11.021-27.945-11.046-28.481    l3.996-0.193c0.006,0.125,0.76,12.609,11.272,27.772l0.952,1.373l-1.182,1.181c-18.439,18.44-24.237,32.878-24.294,33.022    L64.164,99.415z"/><rect height="32.331" width="4" x="62.016" y="62.616"/><path d="M64.014,65.061c-5.267,0-9.551-4.285-9.551-9.551c0-5.267,4.284-9.551,9.551-9.551c5.268,0,9.553,4.285,9.553,9.551    C73.566,60.775,69.281,65.061,64.014,65.061z M64.014,49.958c-3.061,0-5.551,2.49-5.551,5.551c0,3.061,2.49,5.551,5.551,5.551    c3.062,0,5.553-2.49,5.553-5.551C69.566,52.448,67.075,49.958,64.014,49.958z"/></g></g><path d="M94.752,105.4H33.278c-1.104,0-2-0.896-2-2s0.896-2,2-2h61.474c1.104,0,2,0.896,2,2S95.856,105.4,94.752,105.4z"/><g><path d="M35.278,113.932H14.016V93.377h21.263V113.932z M18.016,109.932h13.263V97.377H18.016V109.932z"/><path d="M114.015,113.678H92.752V93.123h21.263V113.678z M96.752,109.678h13.263V97.123H96.752V109.678z"/></g></svg>
            </div>
            <div class="title-sm text-center">
                Title Local
            </div>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae eum perferendis atque, maxime fugiat id distinctio natus quaerat exercitationem dignissimos praesentium consequuntur qui tempore impedit culpa ab officia mollitia deleniti!
            </p>
        </div>
        <div class="col-3">
            <div class="hero-icon">
                <svg enable-background="new 0 0 128 128" height="128px" id="Layer_1" version="1.1" viewBox="0 0 128 128" width="128px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M89.441,33.805H38.59c-1.968,0-3.568-1.6-3.568-3.565V14.006h57.986V30.24C93.008,32.206,91.408,33.805,89.441,33.805z    M39.021,29.805h49.986V18.006H39.021V29.805z"/><g><path d="M64.164,99.415l-1.965-4.259c-0.075-0.163-7.718-16.506-24.341-33.128l-1.124-1.124l0.841-1.349    C48.172,42.561,48.183,32.4,48.182,32.299l4-0.074c0.009,0.441,0.097,10.766-10.384,28.098    c11.877,12.141,19.003,23.753,22.142,29.479c2.847-5.479,9.438-16.377,22.238-29.427c-10.32-15.537-11.021-27.945-11.046-28.481    l3.996-0.193c0.006,0.125,0.76,12.609,11.272,27.772l0.952,1.373l-1.182,1.181c-18.439,18.44-24.237,32.878-24.294,33.022    L64.164,99.415z"/><rect height="32.331" width="4" x="62.016" y="62.616"/><path d="M64.014,65.061c-5.267,0-9.551-4.285-9.551-9.551c0-5.267,4.284-9.551,9.551-9.551c5.268,0,9.553,4.285,9.553,9.551    C73.566,60.775,69.281,65.061,64.014,65.061z M64.014,49.958c-3.061,0-5.551,2.49-5.551,5.551c0,3.061,2.49,5.551,5.551,5.551    c3.062,0,5.553-2.49,5.553-5.551C69.566,52.448,67.075,49.958,64.014,49.958z"/></g></g><path d="M94.752,105.4H33.278c-1.104,0-2-0.896-2-2s0.896-2,2-2h61.474c1.104,0,2,0.896,2,2S95.856,105.4,94.752,105.4z"/><g><path d="M35.278,113.932H14.016V93.377h21.263V113.932z M18.016,109.932h13.263V97.377H18.016V109.932z"/><path d="M114.015,113.678H92.752V93.123h21.263V113.678z M96.752,109.678h13.263V97.123H96.752V109.678z"/></g></svg>
            </div>
            <div class="title-sm text-center">
                Title Local
            </div>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae eum perferendis atque, maxime fugiat id distinctio natus quaerat exercitationem dignissimos praesentium consequuntur qui tempore impedit culpa ab officia mollitia deleniti!
            </p>
        </div>
        <div class="col-3">
            <div class="hero-icon">
            <svg enable-background="new 0 0 128 128" height="128px" id="Layer_1" version="1.1" viewBox="0 0 128 128" width="128px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M89.441,33.805H38.59c-1.968,0-3.568-1.6-3.568-3.565V14.006h57.986V30.24C93.008,32.206,91.408,33.805,89.441,33.805z    M39.021,29.805h49.986V18.006H39.021V29.805z"/><g><path d="M64.164,99.415l-1.965-4.259c-0.075-0.163-7.718-16.506-24.341-33.128l-1.124-1.124l0.841-1.349    C48.172,42.561,48.183,32.4,48.182,32.299l4-0.074c0.009,0.441,0.097,10.766-10.384,28.098    c11.877,12.141,19.003,23.753,22.142,29.479c2.847-5.479,9.438-16.377,22.238-29.427c-10.32-15.537-11.021-27.945-11.046-28.481    l3.996-0.193c0.006,0.125,0.76,12.609,11.272,27.772l0.952,1.373l-1.182,1.181c-18.439,18.44-24.237,32.878-24.294,33.022    L64.164,99.415z"/><rect height="32.331" width="4" x="62.016" y="62.616"/><path d="M64.014,65.061c-5.267,0-9.551-4.285-9.551-9.551c0-5.267,4.284-9.551,9.551-9.551c5.268,0,9.553,4.285,9.553,9.551    C73.566,60.775,69.281,65.061,64.014,65.061z M64.014,49.958c-3.061,0-5.551,2.49-5.551,5.551c0,3.061,2.49,5.551,5.551,5.551    c3.062,0,5.553-2.49,5.553-5.551C69.566,52.448,67.075,49.958,64.014,49.958z"/></g></g><path d="M94.752,105.4H33.278c-1.104,0-2-0.896-2-2s0.896-2,2-2h61.474c1.104,0,2,0.896,2,2S95.856,105.4,94.752,105.4z"/><g><path d="M35.278,113.932H14.016V93.377h21.263V113.932z M18.016,109.932h13.263V97.377H18.016V109.932z"/><path d="M114.015,113.678H92.752V93.123h21.263V113.678z M96.752,109.678h13.263V97.123H96.752V109.678z"/></g></svg>
            </div>
            <div class="title-sm text-center">
                Title Local
            </div>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae eum perferendis atque, maxime fugiat id distinctio natus quaerat exercitationem dignissimos praesentium consequuntur qui tempore impedit culpa ab officia mollitia deleniti!
            </p>
            </div>
        </div>
    </div>
</div>



<!-- this section for featured product -->
<div class="container text-center center spacer">
    <h2 class="title-md" id="product">
        Featured Product
    </h2>
    <div class="center list-style"></div>
<!-- this section for Card -->

<?php foreach($hasil['products'] as $tmpProdduct):?>
<a class="det" href="#" onclick="modalOpen()" data-id="<?= $product[] = $tmpProdduct['id']?>">
    <div class="card">
        <div class="price">
        $<?= $product[] = $tmpProdduct['price']; ?>
        </div>
        <div>
            <img class="card-image" src="<?= $product[] = $tmpProdduct['preview_url']; ?>" alt="" srcset="">
        </div>
        <div class="title-sm margin-top">
            <?= $product[] = $tmpProdduct['name']; ?>
        </div>
    </div>
</a>
<?php endforeach;?>
</div>



<!-- <div class="container text-center center spacer">
    <h2 class="title-md">
        Icon Set
    </h2>
    <div class="center list-style"></div>

<a href="#">
    <div class="card">
        <div class="price">
            $50
        </div>
        <div>
            <img class="card-image" src="img/shot-cropped-1607746450289.png" alt="" srcset="">
        </div>
        <div class="title-sm margin-top">
            Console Game Icon set
        </div>
    </div>
</a>
</div>


<div class="container text-center center spacer">
    <h2 class="title-md">
        User Interface Kit
    </h2>
    <div class="center list-style"></div>

<a href="#">
    <div class="card">
        <div class="price">
            $50
        </div>
        <div>
            <img class="card-image" src="img/shot-cropped-1607746450289.png" alt="" srcset="">
        </div>
        <div class="title-sm margin-top">
            Console Game Icon set
        </div>
    </div>
</a>
</div>

<div class="container text-center center spacer">
    <h2 class="title-md">
        Illustration
    </h2>
    <div class="center list-style"></div>

<a href="#">
    <div class="card">
        <div class="price">
            $50
        </div>
        <div>
            <img class="card-image" src="img/shot-cropped-1607746450289.png" alt="" srcset="">
        </div>
        <div class="title-sm margin-top">
            Console Game Icon set
        </div>
    </div>
</a>
</div>
</div> -->




<!-- this section for Footer -->
<div class="footer spacer">
    <div class="container">
        <div class="row">
            <div class="col-flex margin-content">
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Soluta facere eos officia et mollitia voluptatum eum recusandae repellat ipsam ullam! Autem beatae expedita eius! Nesciunt maiores inventore non tempora expedita.
            </div>
            <div class="col-flex margin-content">
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ducimus ipsum mollitia, beatae odit eligendi eum nulla doloribus ipsam. Enim at suscipit totam asperiores cum non ratione quas aut. Sed, sunt!
            </div>
        </div>
    </div>
</div>




    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
    function modalOpen(){
        document.getElementById('modal').style.visibility = "visible";
    }
    function modalClose(){
        document.getElementById('modal').style.visibility = "hidden";
    }
    $('.det').on('click', function(){
        console.log($(this).data('id'))
    });
   
</script>

<!-- Modal Section -->
    <div class="modal-bg" id="modal">
        <div class="modal-box" >
        <div class="row">
                <div class="col-flex">
                    <img class="detail-img" src="" alt="">
                </div>
                <div class="col-flex">
                    <div class="title-md"><?= $hasil['product']["name"];?></div>
                    <div class="title-sm price-detail"><?= $hasil['product']["price"];?></div>
                    <div class="margin-top line-height"><b class="title-sm">Description</b><br/><?= $hasil['product']["description"];?>
                </div>
            </div>
        </div>
            <div class="close-modal" onclick="modalClose()">
                close
            </div>
            <a href="<?= $hasil['product']["short_url"];?>">      
                <div class="buy-modal">
                    Buy Product
                </div>
            </a>
        </div>
    </div>
</body>




</html>
